package com.thirith.product;

import java.util.Iterator;
import java.util.List;
import java.util.Scanner;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.AnnotationConfiguration;
import org.hibernate.criterion.Restrictions;

import com.thirith.sale.Sale;

public class ProductApp {
	private static SessionFactory factory; 
	public static void main(String[] args){
		try{
	         factory = new AnnotationConfiguration().
	                   configure().
	                   //addPackage("com.xyz") //add package if used.
	                   addAnnotatedClass(Product.class).
	                   buildSessionFactory();
	      }catch (Throwable ex) { 
	         System.err.println("Failed to create sessionFactory object." + ex);
	         throw new ExceptionInInitializerError(ex); 
	      }
	      
	      menu();
	}
	/* MENU */
	public static void menu(){
		
		ProductApp mgr = new ProductApp();
		
		int menu;
		
		out("Menu\n");
		out("1. View All Products\n");
		out("2. Insert Product\n");
		out("3. Search Product\n");
		out("4. Exit\n");
		out("\n");
		Scanner in = getScannerObj();
		out("Please select: ");
		menu = in.nextInt();
		
		switch (menu){
		case 1: 
			mgr.listProducts();
			break;
		case 2:
			insertProduct();
			break;
		case 3:
			searchMenu();
			break;
		case 4: 
			System.out.println("Thank you");
			System.out.println("Good bye!!!");
			System.exit(0);
			break;
		}
		menu();
	}
	
	/* Method to  READ all the products */
	   public void listProducts( ){
	      Session session = factory.openSession();
	      Transaction tx = null;
	      try{
	         tx = session.beginTransaction();
	         Criteria cr = session.createCriteria(Product.class);
	         List products = cr.list();
	         for (Iterator iterator = products.iterator(); iterator.hasNext();){
	            Product product = (Product) iterator.next(); 
	            System.out.print("ID: " + product.getId());
	            System.out.print("  Code: " + product.getCode()); 
	            System.out.print("  Name: " + product.getName()); 
	            System.out.print("  Category: " + product.getCategory()); 
	            System.out.println("  Price: " + product.getPrice()); 
	         }
	         tx.commit();
	      }catch (HibernateException e) {
	         if (tx!=null) tx.rollback();
	         e.printStackTrace(); 
	      }finally {
	         session.close(); 
	      }
	   }
	   
	   /* Select last record */
	   
	   public void viewLastRecord( ){
		      Session session = factory.openSession();
		      Transaction tx = null;
		      try{
		         tx = session.beginTransaction();
		         Query query = session.createQuery("FROM Product ORDER BY id DESC").setMaxResults(1); 
		         List products = query.list();
		         for (Iterator iterator = products.iterator(); iterator.hasNext();){
		            Product product = (Product) iterator.next(); 
		            System.out.print("ID: " + product.getId());
		            System.out.print("  Code: " + product.getCode()); 
		            System.out.print("  Name: " + product.getName()); 
		            System.out.print("  Category: " + product.getCategory()); 
		            System.out.println("  Price: " + product.getPrice()); 
		         }
		         tx.commit();
		      }catch (HibernateException e) {
		         if (tx!=null) tx.rollback();
		         e.printStackTrace(); 
		      }finally {
		         session.close(); 
		      }
		   }
	   
/********************************** Insert **********************************/
	   public static void insertProduct(){
		   
		   ProductApp mgr = new ProductApp();
		   mgr.viewLastRecord();
		   
		   	int id;
			String code,name,cat;
			double price;
			Scanner in = getScannerObj();
			System.out.println("\nPlease Input Id:");
			id = in.nextInt();
			System.out.println("Please Input Code:");
			code = in.next();
			System.out.println("Please Input Name:");
			name = in.next();
			System.out.println("Please Input Category:");
			cat = in.next();
			System.out.println("Please Input Price:");
			price=in.nextDouble();

			insert(id, code, name, cat, price);
			System.out.println("Success");
	   }
	   
	   public static void insert(int id,String code, String name, String cat, double price){
			Session session = factory.openSession();
			session.beginTransaction();
			Product product = new Product();
			product.setId(id);
			product.setCode(code);
			product.setName(name);
			product.setCategory(cat);
			product.setPrice(price);
			session.save(product);
			session.getTransaction().commit();
		}
	   public static Scanner getScannerObj(){
			return new Scanner(System.in);
		}
	
/******************************* End of Insert ********************************/
	   
/********************************** Search **********************************/
	   
	   //Search Menu
	   public static void searchMenu(){
		   int menu;
			ProductApp mgr = new ProductApp();
		   
			System.out.println("Search");
			System.out.println("1. Search By ID");
			System.out.println("2. Search By Code");
			System.out.println("3. Search By Name");
			out("4. Back\n");
			System.out.print("\n");
			Scanner in = getScannerObj();
			System.out.print("Please select: ");
			menu = in.nextInt();
			
			switch (menu){
			case 1: 
				int id;
				out("\n");
				out("Please input ID: ");
				id = in.nextInt();
				mgr.searchById(id);
				break;
			case 2:
				String code;
				out("\n");
				out("Please input Code: ");
				code = in.next();
				mgr.searchByCode(code);
				break;
			case 3: 
				String name;
				out("\n");
				out("Please input Name: ");
				name = in.next();
				mgr.searchByCode(name);
				break;
			case 4:
				menu();
				break;
			}
			out("\n");
			searchMenu();
	   }
	   
	   //Search Product by ID
	   public void searchById(int id){
			Session session = factory.openSession();
			Transaction tx = null;
			try{
		         tx = session.beginTransaction();
		         Query query = session.createQuery("FROM Product WHERE id = :id");
		         query.setParameter("id", id);
		         List employees = query.list();
		         if (employees.size()<=0){
		        	 System.out.println("No Record!!!");
		         }
		         for (Iterator iterator = employees.iterator(); iterator.hasNext();){
		            Product product = (Product) iterator.next(); 
		            System.out.print("ID: " + product.getId());
		            System.out.print("  Code: " + product.getCode()); 
		            System.out.print("  Name: " + product.getName()); 
		            System.out.print("  Category: " + product.getCategory()); 
		            System.out.println("  Price: " + product.getPrice()); 
		         }
		         tx.commit();
		      }catch (HibernateException e) {
		         if (tx!=null) tx.rollback();
		         e.printStackTrace(); 
		      }finally {
		         session.close(); 
		      }
		}
	   
	 //Search Product by Code
	   public void searchByCode(String code){
			Session session = factory.openSession();
			Criteria cr = session.createCriteria(Product.class);
			Transaction tx = null;
			try{
		         tx = session.beginTransaction();
				 cr.add(Restrictions.like("code", code+"%"));
		         List employees = cr.list();
		         if (employees.size()<=0){
		        	 System.out.println("No Record!!!");
		         }
		         for (Iterator iterator = employees.iterator(); iterator.hasNext();){
		            Product product = (Product) iterator.next(); 
		            System.out.print("ID: " + product.getId());
		            System.out.print("  Code: " + product.getCode()); 
		            System.out.print("  Name: " + product.getName()); 
		            System.out.print("  Category: " + product.getCategory()); 
		            System.out.println("  Price: " + product.getPrice()); 
		         }
		         tx.commit();
		      }catch (HibernateException e) {
		         if (tx!=null) tx.rollback();
		         e.printStackTrace(); 
		      }finally {
		         session.close(); 
		      }
		}
	   
	 //Search Product by Name
	   public void searchByName(String name){
			Session session = factory.openSession();
			Criteria cr = session.createCriteria(Product.class);
			Transaction tx = null;
			try{
		         tx = session.beginTransaction();
				 cr.add(Restrictions.like("name", name+"%"));
		         List employees = cr.list();
		         if (employees.size()<=0){
		        	 System.out.println("No Record!!!");
		         }
		         for (Iterator iterator = employees.iterator(); iterator.hasNext();){
		            Product product = (Product) iterator.next(); 
		            System.out.print("ID: " + product.getId());
		            System.out.print("  Code: " + product.getCode()); 
		            System.out.print("  Name: " + product.getName()); 
		            System.out.print("  Category: " + product.getCategory()); 
		            System.out.println("  Price: " + product.getPrice()); 
		         }
		         tx.commit();
		      }catch (HibernateException e) {
		         if (tx!=null) tx.rollback();
		         e.printStackTrace(); 
		      }finally {
		         session.close(); 
		      }
		}
/******************************** End Search ********************************/	   
	   

	   public static void out(String str){
		   System.out.print(str);
	   }
}

package com.thirith.sale;

public class Sale {
	private int saleId;
	private String pCode;
	private String pName;
	private double price;
	private int qty;
	private double tAmount;
	
	public void Sale(){
	}
	
	public int getSaleId(){
		return saleId;
	}
	public void setSaleId(int id){
		saleId = id;
	}
	
	public String getpCode(){
		return pCode;
	}
	public void setpCode(String pCode){
		this.pCode = pCode;
	}
	
	public String getpName(){
		return pName;
	}
	public void setpName(String pName){
		this.pName = pName;
	}
	
	public double getPrice(){
		return price;
	}
	public void setPrice(double price){
		this.price = price;
	}
	
	public int getQty(){
		return qty;
	}
	public void setQty(int qty){
		this.qty = qty;
	}
	
	public double gettAmount(){
		return tAmount;
	}
	public void settAmount(double tAmount){
		this.tAmount = tAmount;
	}
}

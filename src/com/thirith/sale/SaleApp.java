package com.thirith.sale;

import java.util.Iterator;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import com.thirith.product.Product;

public class SaleApp {

	private static SessionFactory factory;
	
	public static void main(String[] args){
		try{
			factory = new Configuration().configure().buildSessionFactory();
		}catch (Throwable ex) {
			// TODO: handle exception
			System.err.println("Failed to create sessionFactory object." + ex);
	         throw new ExceptionInInitializerError(ex);
		}
		
		SaleApp mgr = new SaleApp();
		
		mgr.listSales();
		
//		mgr.searchById(2);
	}
	
	/* Display All Sale */
	public void listSales( ){
	      Session session = factory.openSession();
	      Transaction tx = null;
	      try{
	         tx = session.beginTransaction();
	         List sales = session.createQuery("FROM Sale").list(); 
	         for (Iterator iterator = sales.iterator(); iterator.hasNext();){
	            Sale sale = (Sale) iterator.next(); 
	            System.out.print("ID: " + sale.getSaleId());
	            System.out.print("  Code: " + sale.getpCode());
	            System.out.print("  Name: " + sale.getpName());  
	            System.out.print("  Price: " + sale.getPrice());
	            System.out.print("  Qty: " + sale.getQty());
	            System.out.println("  Total Amount: " + sale.gettAmount());
	         }
	         tx.commit();
	      }catch (HibernateException e) {
	         if (tx!=null) tx.rollback();
	         e.printStackTrace(); 
	      }finally {
	         session.close(); 
	      }
	   }
	
	public void searchById(int id){
		Session session = factory.openSession();
		Transaction tx = null;
		try{
	         tx = session.beginTransaction();
//	         Query query = session.createQuery("FROM Sale WHERE saleId = :saleId");
	         Query query = session.createQuery("FROM Sale");
//	         query.setParameter("saleId", id);
	         List sales = query.list();
	         for (Iterator iterator = sales.iterator(); iterator.hasNext();){
	            Sale sale = (Sale) iterator.next(); 
	            System.out.print("ID: " + sale.getSaleId());
	            System.out.print("  Code: " + sale.getpCode());
	            System.out.print("  Name: " + sale.getpName());  
	            System.out.print("  Price: " + sale.getPrice());
	            System.out.print("  Qty: " + sale.getQty());
	            System.out.println("  Total Amount: " + sale.gettAmount());
	         }
	         tx.commit();
	      }catch (HibernateException e) {
	         if (tx!=null) tx.rollback();
	         e.printStackTrace(); 
	      }finally {
	         session.close(); 
	      }
	}
}
